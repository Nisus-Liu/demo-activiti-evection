package com.itheima.listener;

import com.itheima.service.FlowService;
import com.itheima.utils.SpringContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component // 貌似是 activiti 内部反射构造, 这里注解没用
public class MyTaskListener implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {
        log.info("eventName=={}", delegateTask.getEventName());
        if(EVENTNAME_ASSIGNMENT.equals(delegateTask.getEventName())){
            FlowService flowService = (FlowService)SpringContextUtil.getBean("flowService");
            log.info("flowService={}",flowService);
            flowService.createTaskEvent(delegateTask);
        }
    }
}
