package com.itheima.entity;

import lombok.Data;

import java.util.Date;

@Data
public class FlowInfo {
    private Long id;
    private String flowname;
    private String flowkey;
    private String filepath;
    /**
     * 0-未部署, 1-已部署
     */
    private Integer state;
    private Date createtime;
    private Date updatetime;
}
