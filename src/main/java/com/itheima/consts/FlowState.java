package com.itheima.consts;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p>
 *
 * @author L_J
 * @since 2022/6/8 0:23
 */
@Getter
@AllArgsConstructor
public enum FlowState {
    未部署(0),
    已部署(1),
    ;
    private final Integer code;
}
