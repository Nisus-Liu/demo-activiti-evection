CREATE TABLE `tb_evection`
(
    `id`            bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `userid`        bigint(20) DEFAULT NULL COMMENT '用户ID',
    `evection_name` varchar(255) DEFAULT NULL COMMENT '出差申请单名称',
    `num`           double       DEFAULT NULL COMMENT '出差天数',
    `begin_date`    datetime     DEFAULT NULL COMMENT '预计开始时间',
    `end_date`      datetime     DEFAULT NULL COMMENT '预计结束时间',
    `destination`   varchar(255) DEFAULT NULL COMMENT '目的地',
    `reason`        varchar(255) DEFAULT NULL COMMENT '出差事由',
    `state`         int(2) DEFAULT NULL COMMENT '0-初始录入, 1-开始审批, 2-审批完成',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tb_flow`
(
    `id`         bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `flowname`   varchar(255) DEFAULT NULL COMMENT '流程名称',
    `flowkey`    varchar(255) DEFAULT NULL COMMENT '流程key',
    `filepath`   varchar(255) DEFAULT NULL COMMENT '流程文件路径',
    `state`      int(2) DEFAULT NULL COMMENT '0-未部署, 1-已部署',
    `createtime` datetime(3) DEFAULT CURRENT_TIMESTAMP (3) COMMENT '创建时间',
    `updatetime` datetime(3) DEFAULT CURRENT_TIMESTAMP (3) ON UPDATE CURRENT_TIMESTAMP (3),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tb_sitemessage`
(
    `id`         bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `userid`     bigint(20) DEFAULT NULL COMMENT '用户ID',
    `taskid`     varchar(255)  DEFAULT NULL COMMENT 'activiti Task ID',
    `type`       int(2) DEFAULT NULL COMMENT '消息类型: 1-待办任务',
    `content`    varchar(1024) DEFAULT NULL COMMENT '消息内容',
    `isread`     int(1) DEFAULT NULL COMMENT '是否已读: 0-已读, 1-未读',
    `createtime` datetime      DEFAULT NULL,
    `updatetime` datetime      DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tb_user`
(
    `id`        bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `username`  varchar(255) DEFAULT NULL COMMENT '用户名',
    `password`  varchar(255) DEFAULT NULL COMMENT '密码',
    `email`     varchar(255) DEFAULT NULL COMMENT '邮箱',
    `gender`    int(2) DEFAULT NULL COMMENT '性别',
    `age`       int(3) DEFAULT NULL COMMENT '年龄',
    `role`      varchar(255) DEFAULT NULL COMMENT '角色',
    `rolegroup` varchar(255) DEFAULT NULL COMMENT '用户组',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ---------------------------------------------------------
INSERT INTO `demo-activiti-evection`.`tb_user`(`id`, `username`, `password`, `email`, `gender`, `age`, `role`, `rolegroup`)
VALUES (1, 'zhangfei', '$2a$10$.dYOdQGu1orMczkgRfKDfO9pTApjc.PVN0ckIBK3GY5kAfqDFbB5m', '3436@qq.com', 1, 34, NULL, NULL);
INSERT INTO `demo-activiti-evection`.`tb_user`(`id`, `username`, `password`, `email`, `gender`, `age`, `role`, `rolegroup`)
VALUES (2, 'guanyu', '$2a$10$AqZ7xvA5PnVbaf7.s58tqOSU4DqyBUQBlOyhDCe6cjhjBY0ybgsL6', '436@163.com', 2, 28, NULL, NULL);
INSERT INTO `demo-activiti-evection`.`tb_user`(`id`, `username`, `password`, `email`, `gender`, `age`, `role`, `rolegroup`)
VALUES (3, 'liubei', '$2a$10$rJvGqzsbHjKzAj7sfGTsHeYSa2uDi9t7VfiDpi82Z4ZSo6iZMz1ne', '5436@mt.com', 1, 21, NULL, NULL);
INSERT INTO `demo-activiti-evection`.`tb_user`(`id`, `username`, `password`, `email`, `gender`, `age`, `role`, `rolegroup`)
VALUES (4, 'zhugeliang', '$2a$10$yHTdYhOr2lFEiKQrP3v45ux7qOmlB/FXZ631oQlQT42BWAB9jsFAC', NULL, NULL, NULL, NULL, NULL);
